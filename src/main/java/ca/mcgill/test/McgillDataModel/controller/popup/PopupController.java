package ca.mcgill.test.McgillDataModel.controller.popup;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.controller.PopupFaceletsBean;
import ca.mcgill.test.McgillDataModel.controller.popup.strategy.DischargeToStrategy;
import ca.mcgill.test.McgillDataModel.controller.popup.strategy.DischargedToRehabStrategy;
import ca.mcgill.test.McgillDataModel.main.MainTest;

@ViewScoped
@ManagedBean
public class PopupController implements Serializable {

	/** Serial. */
	private static final long serialVersionUID = 7418523120438751304L;

	static Logger logger = Logger.getLogger(MainTest.class);

	private PopupFaceletsBean parent;

	private boolean show;

	private final Map<DischargeToTypes, DischargeToStrategy> strategyMap = new HashMap<DischargeToTypes, DischargeToStrategy>();

	private String dischargeType;

	@PostConstruct
	public void init() {

		strategyMap
				.put(DischargeToTypes.REHAB, new DischargedToRehabStrategy());
	}

	public PopupFaceletsBean getParent() {
		return parent;
	}

	public void setParent(PopupFaceletsBean parent) {
		this.parent = parent;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public boolean isShowRehabInfo() {

		return DischargeToTypes.REHAB.getId().equals(dischargeType);
	}

	public boolean isShowLongTermInfo() {

		return DischargeToTypes.LONGTERM.getId().equals(dischargeType);
	}

	public String getDischargeType() {

		return dischargeType;
	}

	public void setDischargeType(String state) {

		this.dischargeType = state;
	}

	public void save() {

		logger.debug(">>Trying to save. ");
		 strategyMap.get(DischargeToTypes.REHAB).save();
		 parent.onSaveSubscriptions();
		 setShow(false);
	}
}
