package ca.mcgill.test.McgillDataModel.controller;

import java.io.Serializable;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.datamodel.OrderDataModel;
import ca.mcgill.test.McgillDataModel.main.MainTest;
import ca.mcgill.test.McgillDataModel.model.Order;
import ca.mcgill.test.McgillDataModel.service.OrderService;

@ViewScoped
@ManagedBean
public class LazyDataModelBean implements Serializable {

	private static final long serialVersionUID = -6239437588285327644L;
	static Logger logger = Logger.getLogger(MainTest.class);

	private String name;
	private OrderDataModel odm;
	
	private Integer selectedItem;
	private int selectedPage = 1;

	@EJB
	private OrderService service;

	@PostConstruct
	public void postContruct() {
		logger.debug("Executing postConstruct");
		name = "John";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Order> getOrders() {

		return service.getOrders();
	}

	public OrderDataModel getOrdersDataModel() {

		if (odm == null) {

			odm = new OrderDataModel(service);
		}

		return odm;
	}
	
	public Integer getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(Integer selectedItem) {
		this.selectedItem = selectedItem;
	}

	public int getSelectedPage() {
		return selectedPage;
	}

	public void setSelectedPage(int selectedPage) {
		this.selectedPage = selectedPage;
	}

	public void submit() {
		
		selectedPage = 1;
		System.out.println("submited");
		odm.setQuantity(selectedItem);
	}
}