package ca.mcgill.test.McgillDataModel.controller.popup.strategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.main.MainTest;

@ViewScoped
@ManagedBean
public class DischargedToRehabStrategy implements DischargeToStrategy,
		Serializable {

	/** serial. */
	private static final long serialVersionUID = 3769891606644932992L;

	static Logger logger = Logger.getLogger(MainTest.class);

	private List<String> rehabCenters = new ArrayList<String>();

	@Override
	public void save() {

		logger.debug("** SAVING REHAB CENTER **");
	}

	public List<String> getRehabCenters() {
		if (rehabCenters.isEmpty()) {

			rehabCenters.add("Rehab 1");
			rehabCenters.add("Rehab 2");
			rehabCenters.add("Rehab 3");
		}
		return rehabCenters;
	}

}
