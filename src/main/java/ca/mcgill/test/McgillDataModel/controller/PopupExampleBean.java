package ca.mcgill.test.McgillDataModel.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.main.MainTest;
import ca.mcgill.test.McgillDataModel.model.Person;

@ViewScoped
@ManagedBean
public class PopupExampleBean implements Serializable {

	/** Serial. */
	private static final long serialVersionUID = 588777404998123533L;
	static Logger logger = Logger.getLogger(MainTest.class);

	private int counter = 0;
	private String lNombre = "Nombre:";
	private String lEdad = "Edad:";
	private Person person;
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public Person getPerson() {
		if (person == null) {
			person = new Person();
		}
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getlNombre() {
		return lNombre;
	}

	public void setlNombre(String lNombre) {
		this.lNombre = lNombre;
	}

	public String getlEdad() {
		return lEdad;
	}

	public void setlEdad(String lEdad) {
		this.lEdad = lEdad;
	}

	public void onClickButton() {
		this.counter++;
	}
}
