package ca.mcgill.test.McgillDataModel.controller.popup;

public enum DischargeToTypes {

	HOME ("1", "Home/Community"),
	REHAB("2", "Rehabilitation Centre"),
	LONGTERM("3", "Long-term Care"),
	CONVALECENCE("4", "Convalecence Facility"),
	ANOTHER_HOSPITAL("5", "Another Hospital"),
	ANOTHER_PROVINCE("6", "Another Province"),
	MORGUE ("7", "Morgue"),
	ANOTHER_UNIT ("8", "Another Unit")
	;
	
	private final String id;
	private final String label;

	private DischargeToTypes(String id, String label) {

		this.id = id;
		this.label = label;
	}

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	
}