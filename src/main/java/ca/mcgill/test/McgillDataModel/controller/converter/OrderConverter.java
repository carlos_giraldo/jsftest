package ca.mcgill.test.McgillDataModel.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ca.mcgill.test.McgillDataModel.model.Order;

@FacesConverter(forClass = Order.class)
public class OrderConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String id) {
		
		if (id == null) {
			
			return null;
		}
		

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return (value instanceof Order) ? String.valueOf(((Order) value).getId()) : null;
	}

}
