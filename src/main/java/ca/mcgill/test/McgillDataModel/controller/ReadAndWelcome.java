package ca.mcgill.test.McgillDataModel.controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ReadAndWelcome {

	private String name;

	@PostConstruct
	public void postContruct() {
		name = "John";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
