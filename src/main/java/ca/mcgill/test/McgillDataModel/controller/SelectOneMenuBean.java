package ca.mcgill.test.McgillDataModel.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ca.mcgill.test.McgillDataModel.model.City;
import ca.mcgill.test.McgillDataModel.model.Order;
import ca.mcgill.test.McgillDataModel.service.OrderService;

@ViewScoped
@ManagedBean
public class SelectOneMenuBean {

	private Integer orderId; // +getter +setter
	private Integer cityId;
	
	private Collection<Order> availableItems; // +getter (no setter necessary)
	private Collection<City> cities;
	
	private String name = "Carlos";

	@EJB
	private OrderService service;

	@PostConstruct
	public void init() {
		availableItems = service.getOrders();
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer selectedItem) {
		this.orderId = selectedItem;
	}

	public Collection<Order> getAvailableItems() {
		return availableItems;
	}

	public void setAvailableItems(Collection<Order> availableItems) {
		this.availableItems = availableItems;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Collection<City> getCities() {
		return cities;
	}

	public void setCities(Collection<City> cities) {
		this.cities = cities;
	}
	
	public void loadCities() {
		
		this.cities = new ArrayList<City>();

		if (orderId != null) {
			
			for (int i = 0; i < 10; i++) {

				City newCity = new City();
				newCity.setId(i);
				newCity.setName("Ciudad " + i);
				cities.add(newCity);
			}
		}
	}
}
