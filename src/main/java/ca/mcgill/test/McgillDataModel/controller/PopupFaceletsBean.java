package ca.mcgill.test.McgillDataModel.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.controller.popup.DischargeToTypes;
import ca.mcgill.test.McgillDataModel.controller.popup.PopupController;
import ca.mcgill.test.McgillDataModel.main.MainTest;
import ca.mcgill.test.McgillDataModel.model.mementos.SubscriptionMemento;
import ca.mcgill.test.McgillDataModel.model.mementos.SubscriptionOriginator;

@ViewScoped
@ManagedBean
public class PopupFaceletsBean implements Serializable {

	/** debug. */
	private static final long serialVersionUID = 671528116923655391L;

	static Logger logger = Logger.getLogger(MainTest.class);

	private static final String UNSAVED_MARK = "*";

	private boolean unsaved;

	private SubscriptionOriginator originator;
	private SubscriptionMemento previousSubscription;
	
	@ManagedProperty (value="#{popupController}")
	private PopupController popupController;
	
	public void setPopupController(PopupController popupController) {
		
		this.popupController = popupController;
		this.popupController.setParent(this);
	}

	@PostConstruct
	public void init() {

		logger.debug("Initializing bean.");
		originator = new SubscriptionOriginator();
		previousSubscription = originator.saveToMemento();
		unsaved = false;
		logger.debug("Originator :" + originator.getState());
		logger.debug("Previous Subscription :"
				+ previousSubscription.getState());
	}

	public boolean isUnsaved() {
		logger.debug("Is value unsaved?: " + unsaved);
		return unsaved;
	}

	public static String getUnsavedMark() {
		return UNSAVED_MARK;
	}

	public String getSelectedSubscription() {
		logger.debug("returning subscription value: " + originator.getState());
		return originator.getState();
	}

	public void setSelectedSubscription(String subscription) {
		logger.debug("Setting subscription value: " + subscription);
		this.originator.setState(subscription);
	}

	public SubscriptionOriginator getOriginator() {
		return originator;
	}

	public void setOriginator(SubscriptionOriginator originator) {
		this.originator = originator;
	}

	public String getPreviousSubscription() {
		if (previousSubscription.getState() == null) {
			return "Not selected";
		}
		return previousSubscription.getState();
	}

	public void onChangeSubscriptions() {

		if (originator.getState() == null) {

			if (previousSubscription.getState() == null) {

				unsaved = false;
			} else {

				unsaved = true;
			}
		} else {
			
			if (!originator.getState().equals(previousSubscription.getState())) {
				unsaved = true;
			}
		}

		logger.debug("listener");
		logger.debug("Originator: " + originator);
		logger.debug("previousSubscription: " + previousSubscription);
		popupController.setDischargeType(originator.getState());
		popupController.setShow(true);
	}

	public void onSaveSubscriptions() {

		previousSubscription = originator.saveToMemento();
		unsaved = false;
	}

	public void resetSelection() {
		
		logger.debug("Resetting selected values.");
		originator.setState(previousSubscription.getState());
		unsaved = false;
		popupController.setShow(false);
	}
	
	public DischargeToTypes[] getDischargedToTypes() {
		
		return DischargeToTypes.values();
	}
}
