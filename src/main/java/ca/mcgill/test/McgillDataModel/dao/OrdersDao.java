package ca.mcgill.test.McgillDataModel.dao;

import java.util.Collection;

import ca.mcgill.test.McgillDataModel.model.Order;

public interface OrdersDao {

	Collection<Order> getOrders();

	Collection<Order> getOrders(int first, int rows);

	Collection<? extends Order> getOrders(Integer quantity, int first, int rows);

	int getTotalOrders();
}
