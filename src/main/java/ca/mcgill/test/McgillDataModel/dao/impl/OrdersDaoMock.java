package ca.mcgill.test.McgillDataModel.dao.impl;

import java.util.ArrayList;
import java.util.Collection;

import javax.enterprise.inject.Default;

import ca.mcgill.test.McgillDataModel.dao.OrdersDao;
import ca.mcgill.test.McgillDataModel.model.Order;

@Default
public class OrdersDaoMock implements OrdersDao {
	
//	private static final Random RANDOM_GENERATOR = new Random();
	private int quantity = 100;
	
	private Collection<Order> buildOrdersList(int start, int size) {
		
		return buildOrdersList(quantity, start, size);
	}
	

	private Collection<Order> buildOrdersList(int quantity, int start, int size) {

		Collection<Order> ordersList = new ArrayList<Order>();

		int max = start + size > quantity ? quantity : start + size;
		for (int i = start; i < max; i++)
			ordersList.add(new Order(i, "order " + i));
		return ordersList;
	}

	@Override
	public Collection<Order> getOrders() {
		return buildOrdersList(0, 100);
	}

	@Override
	public Collection<Order> getOrders(int first, int rows) {

		return buildOrdersList(first, rows);
	}

	@Override
	public Collection<? extends Order> getOrders(Integer quantity, int first,
			int rows) {
		
		
		if (quantity != null) {
			
			this.quantity = quantity;
		}
		
		return buildOrdersList(first, rows);
	}

	@Override
	public int getTotalOrders() {
		return quantity;
	}


}
