package ca.mcgill.test.McgillDataModel.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.ExtendedDataModel;
import org.ajax4jsf.model.Range;
import org.ajax4jsf.model.SequenceRange;

import ca.mcgill.test.McgillDataModel.model.Order;
import ca.mcgill.test.McgillDataModel.service.OrderService;

public class OrderDataModel extends ExtendedDataModel<Order> {

	protected SequenceRange cachedRange = null;
	protected Integer cachedRowCount = null;
	protected Integer cachedQuantity = null;
	protected List<Order> cachedItems = new ArrayList<Order>();
	protected Object rowKey;
	private OrderService service;
	private Integer quantity;

	public OrderDataModel(OrderService service) {

		this.service = service;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public Object getRowKey() {
		return rowKey;
	}

	@Override
	public void setRowKey(Object rowKey) {

		this.rowKey = rowKey;
	}

	protected static boolean areEqualRanges(SequenceRange range1,
			SequenceRange range2) {
		if (range1 == null || range2 == null) {
			return range1 == null && range2 == null;
		} else {
			return range1.getFirstRow() == range2.getFirstRow()
					&& range1.getRows() == range2.getRows();
		}
	}

	@Override
	public void walk(FacesContext facesContext, DataVisitor visitor,
			Range range, Object argument) {

		SequenceRange sequenceRange = (SequenceRange) range;
//		if (this.cachedItems.isEmpty()) {
//	
//			if (!areEqualRanges(this.cachedRange, sequenceRange)) {
//				
//				if (quantity == null || !quantity.equals(cachedQuantity)) {
					int first = sequenceRange.getFirstRow();
					int rows = sequenceRange.getRows();
					
					cachedRange = sequenceRange;
					cachedQuantity = quantity;
					cachedItems.clear();
					cachedItems.addAll(service.getOrders(quantity, first, rows));
//				}
//			}
//
//		}
		for (Order actual : cachedItems){
			
			visitor.process(facesContext, actual.getId(), argument);
		}
	}

	@Override
	public int getRowCount() {
		return quantity == null ? 100 : quantity;
	}

	@Override
	public Order getRowData() {
		for (Order actual : cachedItems) {
			if (actual.getId().equals(getRowKey()))
				return actual;
		}
		return null;
	}

	@Override
	public boolean isRowAvailable() {
		return (getRowData() != null);
	}

	@Override
	public int getRowIndex() {

		int rowIndex = -1;

		for (Order actual : cachedItems) {

			rowIndex++;
			if (actual.getId().equals(rowKey))
				break;
		}
		return rowIndex;
	}

	@Override
	public void setRowIndex(int index) {

		setRowKey(cachedItems.get(index).getId());
	}

	@Override
	public void setWrappedData(Object arg0) {

	}

	@Override
	public Object getWrappedData() {
		// TODO Auto-generated method stub
		return null;
	}

}
