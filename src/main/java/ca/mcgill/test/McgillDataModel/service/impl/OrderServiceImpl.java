package ca.mcgill.test.McgillDataModel.service.impl;

import java.util.Collection;

import javax.ejb.Stateless;

import ca.mcgill.test.McgillDataModel.dao.OrdersDao;
import ca.mcgill.test.McgillDataModel.dao.impl.OrdersDaoMock;
import ca.mcgill.test.McgillDataModel.model.Order;
import ca.mcgill.test.McgillDataModel.service.OrderService;

@Stateless
public class OrderServiceImpl implements OrderService {

	private static OrdersDao dao = new OrdersDaoMock();

	public Collection<Order> getOrders() {
		return dao.getOrders();
	}

	@Override
	public Collection<Order> getOrders(int first, int rows) {
		return dao.getOrders(first, rows);
	}

	@Override
	public Collection<? extends Order> getOrders(Integer quantity, int first,
			int rows) {
		return dao.getOrders(quantity, first, rows);
	}

	@Override
	public int getTotalOrders() {

		return dao.getTotalOrders();
	}

}
