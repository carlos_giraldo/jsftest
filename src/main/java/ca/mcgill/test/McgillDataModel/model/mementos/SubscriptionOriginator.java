package ca.mcgill.test.McgillDataModel.model.mementos;

import org.apache.log4j.Logger;

import ca.mcgill.test.McgillDataModel.main.MainTest;

public class SubscriptionOriginator {

	static Logger logger = Logger.getLogger(MainTest.class);

	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		
		logger.debug("Originator: Setting state to " + state);
		this.state = state;
	}

	public SubscriptionMemento saveToMemento() {

		logger.debug("Originator: Saving to Memento.");
		return new SubscriptionMemento(state);
	}
	
	public void restoreFromMemento(SubscriptionMemento memento) {

		state = memento.getState();
		logger.debug("Originator: State after restoring from Memento: " + state);
	}
}
