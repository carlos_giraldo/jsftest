package ca.mcgill.test.McgillDataModel.model.mementos;

public class SubscriptionMemento {

	private final String state;
	
	public SubscriptionMemento(String state) {
		
		this.state = state;
	}

	public String getState() {
		return state;
	}

}
